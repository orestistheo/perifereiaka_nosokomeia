from datetime import datetime
import dash_table
import dash as dash
import dash_bootstrap_components as dbc
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output, State

import dbConn as dbConn
import shared_functions as func
from app import app
from app_components import header, footer

no_info_string = 'Δεν υπάρχουν πληροφορίες για το συγκεκριμένο συνδυασμό'

external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']

select_specialty_query_string = "select specialities.name, specialities.code from specialities"
select_perioxh_query_string = "select perioxi.name, perioxi.code from perioxi"

empty_slot_slider = dbc.FormGroup(
    [
        dbc.Label(
            "Εισάγετε τον ελάχιστο αριθμό των κενών θέσεων που επιθυμείτε",
            html_for="wait_number", width=2),
        dbc.Col(
            dcc.Slider(
                id='empty_slot_slider_id',
                min=0,
                max=20,
                step=1,
                value=1,
            ),
            width=4,

        ),
        dbc.Col(
            html.Div(id='slider-output-container'),
            width=4,

        ),

    ],
    row=True,
    style={'marginTop': 20, 'marginLeft': 20})

required_fields_label = dbc.Label("Υποχρεωτικά πεδία *", width=2)

specialties_list = dbConn.execute_query(select_specialty_query_string)
specialties_options = {}
specialties_options_list = []
for item_component in specialties_list:
    specialties_options = {'label': item_component[0], 'value': str(item_component[1])}
    specialties_options_list.append(specialties_options)

initial_speciality_value = specialties_options_list[0].get('value')

perioxi_list = dbConn.execute_query(select_perioxh_query_string)
perioxi_options = {}
perioxi_options_list = []
for item_component in perioxi_list:
    perioxi_options = {'label': item_component[0], 'value': str(item_component[1])}
    perioxi_options_list.append(perioxi_options)

initial_perioxh_value = perioxi_options_list[0].get('value')

perioxh_dropdown = dbc.FormGroup(
    [
        dbc.Label("Επιλέξτε Περιοχή", html_for="organizations_drop_down", width=2),
        dbc.Col(
            dcc.Dropdown(
                id="perioxh_drop_down",
                options=perioxi_options_list,
                value=None,
                optionHeight=35
            ),
            width=4,
        ),
    ],
    row=True,
    style={'marginTop': 20, 'marginLeft': 20}
)

speciality_dropdown = dbc.FormGroup(
    [
        dbc.Label("Ειδικότητα *", html_for="speciality_drop_down", width=2),
        dbc.Col(
            dcc.Dropdown(
                id="speciality_drop_down",
                options=specialties_options_list,
                value=initial_speciality_value,
                optionHeight=35
            ),
            width=4,
        ),
    ],
    row=True,
    style={'marginTop': 20, 'marginLeft': 20}
)
dbc.FormGroup(
    [
        dbc.Label("Επιλέξτε Περιοχή", html_for="organizations_drop_down", width=2),
        dbc.Col(
            dcc.Dropdown(
                id="perioxh_drop_down",
                options=perioxi_options_list,
                value=None,
                optionHeight=35
            ),
            width=4,
        ),
    ],
    row=True,
    style={'marginTop': 20, 'marginLeft': 20}
)
submit_button = dbc.FormGroup(
    [dbc.Col(width=2),
        dbc.Col(
            html.Button("Submit", id='subm_btn', style={
                'height': 45, 'width': 90, 'marginTop': 20, 'marginLeft': '25%',
                'fontSize': 16, 'borderRadius': 6,
                'backgroundColor': '#6c757d', 'color': 'white'}), width=3)
    ], row=True,
    style={'marginTop': 20, 'marginLeft': 20})

columns = ['Θέσεις', 'Κενές Θέσεις', 'Χρόνος Διαθεσιμότητας', 'Αναμονή', 'Η πρώτη Θέση ανοίγει την ',
           'Τελευταία ενημέρωση', 'Περιοχή', 'Νοσοκομείο']

data_table_component = dash_table.DataTable(
    id='data_table',
    style_table={'height': '200px', 'overflowY': 'auto', 'overflowX': 'auto', 'width': 1000},
    style_header={'whiteSpace': 'normal'},
    columns=[{
        'name': '{}'.format(i),
        'id': '{}'.format(i)
    } for i in columns],
    fixed_rows={'headers': True},
    style_cell={
        'width': 100,
        'textAlign': 'center'
    },
    editable=False
)
initial_datatable_component = data_table_component

# App component containing all html elements and custom style
home_layout = html.Div([
    header,
    dbc.Form(
        [empty_slot_slider, speciality_dropdown, perioxh_dropdown,
         required_fields_label,
         submit_button])
    , html.Div(initial_datatable_component, id='datatable_element')
    , html.Div(id='my-output')
    , footer
])


@app.callback(
    dash.dependencies.Output('slider-output-container', 'children'),
    [dash.dependencies.Input('empty_slot_slider_id', 'value')])
def update_output(value):
    return '{}'.format(value)


@app.callback(
    [Output('datatable_element', 'children'), Output('my-output', 'children')],
    [Input('subm_btn', 'n_clicks')],
    [State('perioxh_drop_down', 'value'), State('speciality_drop_down', 'value'),
     State('empty_slot_slider_id', 'value')])
def search_info_for_submission(clicks, perioxh_code, speciality_code, empty_slots):
    if clicks is None:
        component_to_return = data_table_component
        component_to_return.data = None
        return component_to_return, None
    else:
        if speciality_code is None:
            return None, func.get_validation_div_with_msg('Επιλέξτε μια ειδικότητα')
        if empty_slots is None:
            return None, func.get_validation_div_with_msg('Λάθος αριθμός κενών θέσεων')
        if perioxh_code is None:
            summary_query_string = "select id,slots, empty_slots, time_available, waiting, first_slot_opens, last_update, perioxi, nosokomeio from tot_perifereiaka WHERE tot_perifereiaka.speciality_code='{speciality_code}' and (tot_perifereiaka.empty_slots >= {empty_slots} or tot_perifereiaka.empty_slots is NULL)   ".format(
                empty_slots=empty_slots, speciality_code=str(speciality_code))
        else:
            summary_query_string = "select id, slots, empty_slots, time_available, waiting, first_slot_opens, last_update, perioxi, nosokomeio from tot_perifereiaka WHERE tot_perifereiaka.speciality_code='{speciality_code}' and tot_perifereiaka.perioxi_code='{perioxh_code}' and (tot_perifereiaka.empty_slots >= {empty_slots} or tot_perifereiaka.empty_slots is NULL)  ".format(
                perioxh_code=str(perioxh_code), empty_slots=empty_slots, speciality_code=str(speciality_code))
        data_frame = dbConn.execute_query_to_pandas(summary_query_string)
        if data_frame is None or len(data_frame) == 0:
            return None, "Δεν υπάρχουν πληροφορίες για το συγκεκριμένο μέρος"

        slots_list = data_frame["slots"]
        empty_slots_list = data_frame["empty_slots"]
        time_available_list = data_frame["time_available"]
        waiting_list = data_frame["waiting"]
        first_slot_opens_list = data_frame["first_slot_opens"]
        last_update_list = data_frame["last_update"]
        perioxh_list = data_frame["perioxi"]
        nosokomeio_list = data_frame["nosokomeio"]

        columns = ['Θέσεις', 'Κενές Θέσεις', 'Χρόνος Διαθεσιμότητας', 'Αναμονή', 'Η πρώτη Θέση ανοίγει την ',
                   'Τελευταία ενημέρωση', 'Περιοχή', 'Νοσοκομείο']

        data_frame[columns[0]] = slots_list
        data_frame[columns[1]] = empty_slots_list
        data_frame[columns[2]] = time_available_list
        data_frame[columns[3]] = waiting_list
        data_frame[columns[4]] = first_slot_opens_list
        data_frame[columns[5]] = last_update_list
        data_frame[columns[6]] = perioxh_list
        data_frame[columns[7]] = nosokomeio_list
        data_frame = data_frame[
            ['Θέσεις', 'Κενές Θέσεις', 'Χρόνος Διαθεσιμότητας', 'Αναμονή', 'Η πρώτη Θέση ανοίγει την ',
             'Τελευταία ενημέρωση', 'Περιοχή', 'Νοσοκομείο']]
        component_to_return = data_table_component
        if data_frame is None:
            component_to_return.data = None
            return component_to_return
        component_to_return.data = data_frame.to_dict('rows')
        return component_to_return, None
